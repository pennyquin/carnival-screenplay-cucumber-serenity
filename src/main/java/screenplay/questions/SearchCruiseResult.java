package screenplay.questions;

import net.serenitybdd.screenplay.questions.targets.TargetText;
import net.serenitybdd.screenplay.questions.targets.TheTarget;
import screenplay.user_interfaces.SearchCruisesForm;


public class SearchCruiseResult {

    public static TargetText submitFirstResult(){
        return TheTarget.textOf(SearchCruisesForm.FIRST_RESULT_ITEM);
    }

    public static TargetText submitFirstResultDays(){
        return TheTarget.textOf(SearchCruisesForm.FIRST_RESULT_DURATION_ITEM);
    }

    public static TargetText defaultPassengerCount(){
        return TheTarget.textOf(SearchCruisesForm.TRAVELERS_COUNT_INPUT);
    }

    public static TargetText resultsTotal(){
        return TheTarget.textOf(SearchCruisesForm.RESULTS_TOTAL);
    }


}
