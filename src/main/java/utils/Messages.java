package utils;

public class Messages {

    public static final String INVALID_CREDENTIALS = "We're sorry but the credentials entered do not match.";
    public static final String MISSING_EMAIL = "E-mail/username is required";
    public static final String MISSING_PASSWORD = "Password is required";
    public static final String INVALID_USERNAME_TITLE = "Request validation failed.";
    public static final String INVALID_USERNAME_VALUE = "Username is invalid.";
}
