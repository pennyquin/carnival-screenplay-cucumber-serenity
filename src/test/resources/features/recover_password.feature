Feature: El usuario quiere recuperar su contraseña
Scenario: Usuario intenta recuperar su contraseña con datos inválidos
Given Pepito no es un usuario registrado
And el quiere intentar recuperar una contraseña
When él envíe la información requerida para la recuperación de la contraseña
Then él debería ser informado que la operación no pudo ser realizada

Scenario: Usuario intenta recuperar su contraseña con datos válidos
Given Pepito es un usuario registrado
And el quiere intentar recuperar una contraseña
When él envíe la información valida requerida para la recuperación de la contraseña
Then él debería ser informado que la operación pudo ser realizada