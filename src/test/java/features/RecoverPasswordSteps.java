package features;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abiities.CallAnApi;
import screenplay.abilities.SearchCruises;
import screenplay.questions.SearchCruiseResult;
import screenplay.tasks.FillPassengerCount;
import screenplay.tasks.FillSearchCriteria;
import screenplay.tasks.RecoverPassword;
import screenplay.tasks.Start;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

public class RecoverPasswordSteps {

    private static final String VALID_EMAIL= "pendmqg@gmail.com";
    private static final String INVALID_EMAIL= "pennyxz";
    private static final String API_URL= "http://localhost:8080";


    @Before
    public void setTheStageScreenplay(){
        RestAssured.registerParser("text/plain", Parser.TEXT);
        OnStage.setTheStage(new OnlineCast());
    }


    @Given("^(.*) (.*)?es un usuario registrado$")
    public void call_actor(String actor, String no){
        theActorCalled(actor);

    }

    @When("^el quiere intentar recuperar una contraseña$")
    public void try_to_recover_password(){
        theActorInTheSpotlight()
                .whoCan(CallAnApi.at(API_URL));
    }

    @And("^él envíe la información requerida para la recuperación de la contraseña$")
    public void send_invalid_info_to_recover_password(){
        theActorInTheSpotlight()
                .attemptsTo(RecoverPassword.withEmail(INVALID_EMAIL));
    }

    @And("^él envíe la información valida requerida para la recuperación de la contraseña$")
    public void send_valid_info_to_recover_password(){
        theActorInTheSpotlight()
                .attemptsTo(RecoverPassword.withEmail(VALID_EMAIL));
    }

    @Then("^él debería ser informado que la operación no pudo ser realizada$")
    public void user_should_be_able_to_see_that_is_not_possible(){
        theActorInTheSpotlight()
                .should(seeThatResponse(response -> response.statusCode(400).body(is("Your unable to recover your password since you are not registered"))));

    }

    @Then("^él debería ser informado que la operación pudo ser realizada$")
    public void user_should_be_able_to_see_that_is_possible(){
        theActorInTheSpotlight()
                .should(seeThatResponse(response -> response.statusCode(200).body(is("We are going to send a link to your email with the new password"))));

    }

}
