package screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.Step;
import screenplay.abilities.SearchCruises;
import screenplay.questions.SearchCruiseResult;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static screenplay.user_interfaces.SearchCruisesForm.*;

public class FillPassengerCount implements Task {

    @Step("Search Cruises loaded {0}")
    public static FillPassengerCount withPreferences() {
        return instrumented(FillPassengerCount.class);
    }

    @Step
    public <T extends Actor> void performAs(T user) {
        user.attemptsTo(
                Click.on(TRAVELERS_BUTTON)
        );

        Target button;
        Integer defaultValue = Integer.valueOf(SearchCruiseResult.defaultPassengerCount().answeredBy(user).trim());
        Integer numberTravelers = Integer.valueOf(preferences(user).getNumberOfTravelers());
        if (numberTravelers > defaultValue) {
            button = TRAVELERS_COUNT_UP;
        } else {
            button = TRAVELERS_COUNT_DOWN;
        }

        for (int i = 0; i < Math.abs(defaultValue - numberTravelers); i++) {
            user.attemptsTo(
                    Click.on(button)
            );
        }

        user.attemptsTo(
                Click.on(TRAVELERS_BUTTON)
        );

    }

    private SearchCruises preferences(Actor actor) {
        return SearchCruises.as(actor);
    }

}
