
# Carnival Testing con Cucumber + Serenity + Screenplay Pattern
Este proyecto realiza pruebas e2e y de API especificado en la actividad.
Contiene los ejercicios 1,2 y 4.

## Compilación de preubas

Este es un proyecto Maven. Para ejecutar pruebas

`mvn clean verify` -> Pruebas con Chrome
`mvn clean verify -Dwebdriver.driver=firefox` -> Pruebas con Firefox
