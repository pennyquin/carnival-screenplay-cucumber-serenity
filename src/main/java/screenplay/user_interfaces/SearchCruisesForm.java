package screenplay.user_interfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class SearchCruisesForm extends PageObject {


    public static Target SHIPS_MENU = Target.the("Click the ships menu")
            .locatedBy("//a[@title='Ships']");
    public static Target DURATION_BUTTON = Target.the("Click the duration button")
            .locatedBy("#cdc-durations");
    public static Target TRAVELERS_BUTTON = Target.the("Click the Number of travelers button")
            .locatedBy("//a[@title='Number of Travelers']");
    public static Target TRAVELERS_COUNT_INPUT = Target.the("Fill the travelers count")
            .locatedBy("//span[@class='xft-travelers-group__count ng-binding']");

    public static Target TRAVELERS_COUNT_UP = Target.the("Fill the travelers count")
            .locatedBy("//i[@class='fa fa-caret-up']");

    public static Target TRAVELERS_COUNT_DOWN = Target.the("Fill the travelers count")
            .locatedBy("//i[@class='fa fa-caret-down']");

    public static Target FIRST_RESULT_ITEM = Target.the("the first result")
            .locatedBy("//div[@class='vrg-search-unit'][0]");

    public static Target FIRST_RESULT_DURATION_ITEM = Target.the("See the first result duration")
            .locatedBy( "(//span[@class='cgc-cruise-glance__days ng-binding'])[0]");

    public static Target RESULTS_TOTAL = Target.the("the of results")
            .locatedBy( "//span[@class='sbsc-container__result-count ng-binding']");



    public static Target getSubMenuSpecificButton(String withText){

        return Target.the("Click the "+withText+" button")
                .locatedBy("//button[@class='cdc-filter-button ng-binding' and contains(text(), '"+withText+"')]");
    }


}
