package features;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import screenplay.abilities.SearchCruises;
import screenplay.questions.SearchCruiseResult;
import screenplay.tasks.FillPassengerCount;
import screenplay.tasks.FillSearchCriteria;
import screenplay.tasks.Start;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.not;

public class SearchCruiseSteps {

    private String duration;

    @Before
    public void setTheStageScreenplay(){
        OnStage.setTheStage(new OnlineCast());
    }


    @Given("^(.*) quiere realizar la reserva del crucero de sus sueños$")
    public void opens_the_cruise_search(String actor){
        theActorCalled(actor)
                .wasAbleTo(Start.goToSearchCruises());
    }

    @When("^él seleccione el barco (.*), la duracion (.*)$")
    public void select_ship_and_duration(String barco, String duracion){
        theActorInTheSpotlight()
                .whoCan(SearchCruises.with(barco, duracion))
                .attemptsTo(FillSearchCriteria.withPreferences());
    }

    @And("^él seleccione el número de pasajeros (.*)$")
    public void select_passenger_number(String pasajeros){
        SearchCruises ability = theActorInTheSpotlight().abilityTo(SearchCruises.class);
        theActorInTheSpotlight().whoCan(SearchCruises.with(ability, pasajeros))
                .attemptsTo(FillPassengerCount.withPreferences());
    }

    @Then("^él debería visualizar que el primer resultado coincide con los criterios de la búsqueda$")
    public void user_should_be_able_to_see_result_criteria(){
        theActorInTheSpotlight()
                .should(seeThat(SearchCruiseResult.resultsTotal(),
                        not("0")));
    }

}
