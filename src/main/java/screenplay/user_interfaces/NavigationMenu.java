package screenplay.user_interfaces;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.pages.PageObject;

public class NavigationMenu extends PageObject {

    private static Target GET_LOGIN_SCREEN =Target.the("Log In Button")
            .locatedBy("#ccl_header_expand-login-link");

    private static Target GET_SEARCH_CRUISE_SCREEN =Target.the("Search Cruise Button")
            .locatedBy("//a[@href='/cruise-search']");

    public static Target getLoginScreen() {
        return GET_LOGIN_SCREEN;
    }

    public static Target getSearchCruiseScreen() {
        return GET_SEARCH_CRUISE_SCREEN;
    }
}
