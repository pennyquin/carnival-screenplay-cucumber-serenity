package screenplay.user_interfaces;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.targets.Target;

public class LoginForm extends PageObject {

    public static Target FILL_USERNAME = Target.the("Fill E-mail Address or Username")
            .locatedBy("#username");
    public static Target FILL_PASSWORD = Target.the("Fill Password")
            .locatedBy("#password");
    public static Target SIGN_IN = Target.the("Click on LOG IN!")
            .locatedBy("//button[@type='submit']");
    public static Target INVALID_LOGIN_NOTIFICATOR = Target.the("Submit State Label")
            .locatedBy("//li[@class='errf-item']");
    public static Target EMAIL_REQUIRED_LABEL = Target.the("Email validator Label")
            .locatedBy("#username-ffr-val-msg-0");
    public static Target PASSWORD_REQUIRED_LABEL = Target.the("Password validator Label")
            .locatedBy("#password-ffr-val-msg-0");
}
