package screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Step;
import screenplay.abilities.SearchCruises;
import screenplay.user_interfaces.SearchCruisesForm;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static screenplay.user_interfaces.SearchCruisesForm.DURATION_BUTTON;
import static screenplay.user_interfaces.SearchCruisesForm.SHIPS_MENU;

public class FillSearchCriteria implements Task {

    @Step("Search Cruises loaded {0}")
    public static FillSearchCriteria withPreferences(){
        return instrumented(FillSearchCriteria.class);
    }

    @Step
    public <T extends Actor> void performAs(T user) {
        user.attemptsTo(
                Click.on(SHIPS_MENU),
                Click.on(SearchCruisesForm.getSubMenuSpecificButton(preferences(user).getShipName())),
                Click.on(DURATION_BUTTON),
                Click.on(SearchCruisesForm.getSubMenuSpecificButton(preferences(user).getDuration()))
        );
    }

    private SearchCruises preferences(Actor actor) {
       return SearchCruises.as(actor);
    }

}
