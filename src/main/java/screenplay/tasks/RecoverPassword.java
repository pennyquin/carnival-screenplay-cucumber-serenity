package screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.thucydides.core.annotations.Step;
import screenplay.abilities.Authenticate;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static screenplay.user_interfaces.LoginForm.*;

public class RecoverPassword implements Task {

    private static final String RESOURCE= "/api/users/forgot-password/{email}";
    private final String email;

    public RecoverPassword(String email){
        this.email = email;
    }

    @Step()
    public static RecoverPassword withEmail(String email){
        return instrumented(RecoverPassword.class, email);
    }

    @Step("{0} try to recover password with email #email")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to(RESOURCE).with(request -> request.pathParam("email", email))
        );
    }

}
