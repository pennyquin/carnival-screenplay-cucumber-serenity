Feature: El usuario quiere buscar el crucero de sus sueños
Scenario Outline: Usuario busca el crucero de sus sueños

Given Pepito quiere realizar la reserva del crucero de sus sueños
When él seleccione el barco <barco>, la duracion <duracion>
And él seleccione el número de pasajeros <pasajeros>
Then él debería visualizar que el primer resultado coincide con los criterios de la búsqueda
Examples:
|barco |duracion| pasajeros |
|Carnival Conquest| 2 - 5 | 1 |
|Carnival Fantasy | 10+ | 5 |