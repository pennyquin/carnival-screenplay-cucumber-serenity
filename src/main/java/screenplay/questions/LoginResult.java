package screenplay.questions;

import net.serenitybdd.screenplay.questions.targets.TargetText;
import net.serenitybdd.screenplay.questions.targets.TargetTextValues;
import net.serenitybdd.screenplay.questions.targets.TheTarget;
import screenplay.user_interfaces.LoginForm;


public class LoginResult {

    public static TargetText submitResult(){
        return TheTarget.textOf(LoginForm.INVALID_LOGIN_NOTIFICATOR);
    }

    public static TargetText emailValidation(){
        return TheTarget.textOf(LoginForm.EMAIL_REQUIRED_LABEL);
    }

    public static TargetText passwordValidation(){
        return TheTarget.textOf(LoginForm.PASSWORD_REQUIRED_LABEL);
    }

    public static TargetTextValues usernameValidation(){
        return TheTarget.textValuesOf(LoginForm.INVALID_LOGIN_NOTIFICATOR);
    }

}
