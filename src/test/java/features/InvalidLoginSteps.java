package features;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import screenplay.abilities.Authenticate;
import screenplay.questions.LoginResult;
import screenplay.tasks.LogIn;
import screenplay.tasks.Start;
import utils.Messages;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;

public class InvalidLoginSteps {

    @Before
    public void setTheStageScreenplay(){
        OnStage.setTheStage(new OnlineCast());
    }


    @Given("^(.*) abre la pagina principal y da click en Login$")
    public void opens_login_form(String actor){
        theActorCalled(actor)
                .wasAbleTo(Start.prepareToSignIn());
    }

    //TODO USE PARAMETERS INSTEAD
    @When("^ingresa un usuario (.*) y contraseña (.*) invalidos$")
    public void fills_with_invalid_credentials(String usuario, String password){
        theActorInTheSpotlight()
                .whoCan(Authenticate.with(usuario, password))
                .attemptsTo(LogIn.withCredentials());
    }

    @Then("^deberia visualizar en pantalla una alerta de credenciales invalidas$")
    public void user_should_be_able_to_see_he_messed_up(){
        theActorInTheSpotlight()
                .should(seeThat(LoginResult.submitResult(),
                        containsString(Messages.INVALID_CREDENTIALS)));
    }

    @When("^no ingresa nada para usuario (.*) y contraseña (.*)$")
    public void fills_with_no_credentials(String usuario, String password){
        theActorInTheSpotlight()
                .whoCan(Authenticate.with(usuario, password))
                .attemptsTo(LogIn.withCredentials());
    }

    @Then("^deberia visualizar en pantalla una alerta de datos faltantes$")
    public void user_should_be_able_to_see_he_messed_up_again(){
        theActorInTheSpotlight()
                .should(seeThat(LoginResult.emailValidation(),
                        containsString(Messages.MISSING_EMAIL)));
        theActorInTheSpotlight()
                .should(seeThat(LoginResult.passwordValidation(),
                        containsString(Messages.MISSING_PASSWORD)));
    }

    @When("^ingresa un valor invalido para usuario (.*) y contraseña (.*)$")
    public void fills_with_invalid_username(String usuario, String password){
        theActorInTheSpotlight()
                .whoCan(Authenticate.with(usuario, password))
                .attemptsTo(LogIn.withCredentials());
    }

    @Then("^deberia visualizar en pantalla una alerta de usuario invalido$")
    public void user_should_be_able_to_see_he_messed_up_one_more_time(){
        theActorInTheSpotlight()
                .should(seeThat(LoginResult.usernameValidation(),
                        contains(Messages.INVALID_USERNAME_TITLE, Messages.INVALID_USERNAME_VALUE)));

    }

}
