package screenplay.tasks;

import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static screenplay.user_interfaces.NavigationMenu.getSearchCruiseScreen;

public class GoToSearchCruises {

     public static Task called () {
       return Task.where("Go To Search cruises screen",
               Click.on(getSearchCruiseScreen()));
    }
}
