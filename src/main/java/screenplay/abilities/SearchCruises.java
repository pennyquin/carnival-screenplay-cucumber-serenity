package screenplay.abilities;

import net.serenitybdd.screenplay.Ability;
import net.serenitybdd.screenplay.Actor;

public class SearchCruises implements Ability {

    private String shipName;
    private String duration;
    private String numberOfTravelers;

    private SearchCruises() {
    }

    public static class Builder {


        private String shipName;
        private String duration;
        private String numberOfTravelers;

        public Builder() {

        }

        public Builder shipName(String shipname) {
            this.shipName = shipname;
            return this;
        }

        public Builder duration(String duration) {
            this.duration = duration;
            return this;
        }

        public Builder numberOfTravelers(String numberOfTravelers) {
            this.numberOfTravelers = numberOfTravelers;
            return this;
        }

        public SearchCruises build() {
            SearchCruises searchCruises = new SearchCruises();
            searchCruises.shipName = this.shipName;
            searchCruises.duration = this.duration;
            searchCruises.numberOfTravelers = this.numberOfTravelers;
            return searchCruises;
        }

    }

    public String getShipName() {
        return shipName;
    }

    public String getDuration() {
        return duration;
    }

    public String getNumberOfTravelers() {
        return numberOfTravelers;
    }

    public static SearchCruises with(String shipName, String duration){
        return new SearchCruises.Builder().shipName(shipName).duration(duration).build();
    }

    public static SearchCruises with(SearchCruises searchCruises, String passengerCount){
        return new Builder().shipName(searchCruises.shipName).duration(searchCruises.duration).numberOfTravelers(passengerCount).build();
    }

    public static SearchCruises as(Actor actor) {
        return actor.abilityTo(SearchCruises.class);
    }

}
