Feature: El usuario puede navegar e intentar hacer login en la pagina
Pepito quiere intentar hacer login dentro de la pagina
de crucero para realizar reservas personalizadas

Scenario Outline: Login fallido por credenciales invalidas
Given Pepito abre la pagina principal y da click en Login
When ingresa un usuario <usuario> y contraseña <password> invalidos
Then deberia visualizar en pantalla una alerta de credenciales invalidas
Examples:
| usuario                              | password |
| pendmqg@gmail.com                    | 1234567 |

Scenario Outline: Login fallido por no ingresar credenciales
Given Pepito abre la pagina principal y da click en Login
When no ingresa nada para usuario <usuario> y contraseña <password>
Then deberia visualizar en pantalla una alerta de datos faltantes
Examples:
| usuario                              | password |
|                                      |          |

Scenario Outline: : Login fallido por usuario invalido
Given Pepito abre la pagina principal y da click en Login
When ingresa un valor invalido para usuario <usuario> y contraseña <password>
Then deberia visualizar en pantalla una alerta de usuario invalido
Examples:
| usuario                              | password |
| pendmqg@gmail.com pendmqg@gmail.com    |  1234        |
